
$(function() {
	
	// init buttons active
	$(".form_bttn").each(function() {
		$(this).mousedown(function() {
			$(this).addClass("active");
		}); 
		$(this).bind("mouseup mouseout", function() {
			$(this).removeClass("active");
		})
	});

	// init popups 
	$("[rel^='popup']").click(function(e) {
		var popup = "#" + $(this).attr("rel");
		$(popup).lightbox_me({
	        centered: true, 
	        overlayCSS: {
	        	background: "#2e9332",
	        	opacity: .8
	        },
	        closeSelector: ".popup__close"
        });
		e.preventDefault();
	});

	// init placeholder
	$("input[placeholder], textarea[placeholder]").placeholder();

	// init menu
	$(".menu__item").hover(function() {
		$(this).find(".submenu").stop().fadeIn(200);
	}, function() {
		$(this).find(".submenu").stop().fadeOut(100);
	});

	function resizeSliderBuffer() {
		var plusesH  = $(".about__main").height() + $(".pluses").height() - 
				$(".factory-c").height() - 54;
		var sliderH = 102 + $(".slider__item:visible").height() - 
				$(".product__c").parent().height()
		$(".pluses-buffer").height(plusesH);
		$(".slider-buffer").height(sliderH);
	}

	// init pluses buffer
	$(window).load(function() {
		resizeSliderBuffer();
	}).resize(function() {
		resizeSliderBuffer();
	});

	// init fancybox
	$(".c-fancybox").fancybox();

});


// init slider
$(function() {
	var intervalId = null;
	function startAnimating() {
		intervalId = setInterval(function() {
			animate();
		}, 9000);
	}
	function stopAnimating() {
		clearInterval(intervalId);
	}
	function animate() {
		var copyWNew;
		var item  = $(".slider__item:visible");
		var itemW = item.width();
		var copy  = item.find(".slider__copy");
		if (copy.is(":animated")) return;
		var copyW = copy.width();
		if (copyW >= itemW/2) {
			copyWNew = 0;
		} else {
			copyWNew = itemW;
		}
		copy.animate({
			width: copyWNew
		}, 1500);
	}
	function animateFirst() {
		setTimeout(function() {
			$(".slider__item:visible .slider__copy").animate({
				width: "90%"
			});
		}, 100);
	}
	function setToDefault() {
		stopAnimating();
		$(".slider__copy").stop().css({
			width: 0,
			overflow: "visible"
		});
	}
	function resizeImages() {
		var source = $(".slider__item:visible");
		$(".slider__copy img").css({
			width:  source.width(),
			height: source.height()
		});
	}
	$(window).load(function() {
		resizeImages();
		animateFirst();
		startAnimating();
	}).resize(function() {
		resizeImages();
	});
	$(".slider__bttn").click(function(e) {
		e.preventDefault();
		if ($(this).hasClass("active")) return;
		var index = $(this).index();
		$(".active").removeClass("active")
		$(this).addClass("active");
		setToDefault();
		$(".slider__item").hide().eq(index).stop().fadeIn(function() {
			animateFirst();
			startAnimating();
		});
	});
	$(".slider__item").mousemove(function(e) {
		var copy  = $(this).find(".slider__copy");
		var visor = $(this).find(".slider__visor");
		var xMax  = $(this).width();
		var x = e.pageX - $(this).offset().left;
		if (x > xMax - 20) x = xMax - 20;
		else if (x < 20) x = 20;
		if (x > xMax - 100) visor.hide();
		else visor.show();
		copy.width(x);
	}).hover(function() {
		setToDefault();
		$(this).find(".slider__visor").show();
	}, function() {
		startAnimating();
		$(this).find(".slider__visor").hide();
	});
})


// init reviews 
$(function() {

	function showDesc() {
		var title = $(".roundabout-in-focus").attr("data-title");
		$(".side-reviews__footer").html(title).stop().fadeIn(200);
	}
	$(".side-reviews__next, .side-reviews__prev").click(function() {
		$(".side-reviews__footer").stop().fadeOut();
	});
	$("#side-reviews").roundabout({
		btnNext: $(".side-reviews__next"),
		btnPrev: $(".side-reviews__prev"),
		btnPrevCallback: function() {
			showDesc();
		},
		btnNextCallback: function() {
			showDesc();
		},
		clickToFocusCallback: function() {
			showDesc();
		}
	}, function() {
		showDesc();
		$(".side-reviews__next, .side-reviews__prev").fadeIn();
	});


});